# Aim of lernaYana
A website to teach naYana Script in an interactive and fun way with the use of gamification elements. Gamification In web design seeks to include scoring points, clearing levels, accomplishing objectives, and even competition.

## About naYana Script
 - The naYana phonetic alphabet is created by Nagarjuna G. and Vickram Crishna and few other collaborators and interns at the gnowledge lab (https://www.gnowledge.org) of Homi Bhabha Centre for Science Education (https://www.hbcse.tifr.res.in), Tata Institute of Fundamental Research (TIFR) at Mumbai in India.
- Common code does not eliminate diversity of expression within a wider population, on the other hand it becomes a base for inclusive participation. Trascriptional unity can generate translational diversity is well evidenced by a common genetic code, where four letters and 64 words generated the organic diversity which is key for organic evolution. We hope naYana project will enhance cultural diversity and localization through transciptional unity and universal literacy.
- For further information on naYana, visit https://www.gnowledge.org/projects/naYana
## Features of lernaYana
Some features implemented in the website are as follows:
 - Tracing over the naYana script shapes and corresponding pronounciation
 - Making levels (game-like) to choose character and its sound, vice versa
 - Table with all naYana script letters, examples and pronounciation
 - User login and saving progress

